# void-custom-packages

paquetes que no estan disponibles en el repositorio principal de void-packages

## Como funciona?

Si no tienes el repositorio local de void-packages, clonalo(**si lo tienes, omite este paso**):

```
git clone --depth 1 https://github.com/void-linux/void-packages.git

cd void-packages

./xbps-src binary-bootstrap
```

Luego clona mi repositorio y pasa todas las plantillas al repositorio local de void-packages:

```
cd && git clone --depth 1 https://gitlab.com/d33vliter/void-custom-packages.git

cd void-custom-packages && cp -r srcpkgs/* ~/void-packages/srcpkgs/
```

# Listo!, a compilar :p
